import { Component, OnInit } from '@angular/core';
import {Product} from "../interfaces/product";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";
import {Stock} from "../interfaces/stock";


@Component({
  selector: 'app-disburse',
  templateUrl: './disburse.component.html',
  styleUrls: ['./disburse.component.css']
})
export class DisburseComponent implements OnInit {

  stocks : [Stock];

  departments : [Product];

  disburseForm : FormGroup;

  stockId   : FormControl;

  departmentId : FormControl;

  quantity : FormControl;

  recipient : FormControl;


  constructor(private builder: FormBuilder,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getStocks();
    this.getDepartments();
    this.makeDisburseForm();
  }

  getDepartments(){
    this.inventoryHttpService.getDepartments().subscribe(departments => {
      this.departments = departments;
    }, error => {
      this.toasterService.pop('error', 'Error', error)
    });
  }

  disburse(disburseData){
    this.inventoryHttpService.disburse(disburseData).subscribe(data => {
      this.toasterService.pop('success', 'Created',  data.message);
      this.disburseForm.reset();
    }, error => {
      error.forEach(err => {
        this.toasterService.pop('error', 'Error', err);
      });
    })
  }

  getStocks(){
    this.inventoryHttpService.getStocks().subscribe(stocks => {
      this.stocks = stocks;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

  makeDisburseForm(){
    this.stockId = new FormControl('', [
      Validators.required
    ]);
    this.departmentId = new FormControl('', [
      Validators.required
    ]);

    this.quantity = new FormControl('', [
      Validators.required
    ]);

    this.recipient = new FormControl('', [
      Validators.required
    ]);

    this.disburseForm = this.builder.group({
      stockId : this.stockId,
      departmentId     : this.departmentId,
      quantity        : this.quantity,
      recipient       : this.recipient
    });
  }
}
