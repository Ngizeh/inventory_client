import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDisbursedComponent } from './edit-disbursed.component';

describe('EditDisbursedComponent', () => {
  let component: EditDisbursedComponent;
  let fixture: ComponentFixture<EditDisbursedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDisbursedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDisbursedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
