import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisbursedStockComponent } from './disbursed-stock.component';

describe('DisbursedStockComponent', () => {
  let component: DisbursedStockComponent;
  let fixture: ComponentFixture<DisbursedStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisbursedStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisbursedStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
