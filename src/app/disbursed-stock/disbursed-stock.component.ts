import { Component, OnInit } from '@angular/core';
import {DisbursedStock} from "../interfaces/disbursed-stock";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";


@Component({
  selector: 'app-disbursed-stock',
  templateUrl: './disbursed-stock.component.html',
  styleUrls: ['./disbursed-stock.component.css']
})
export class DisbursedStockComponent implements OnInit {

  disbursedStocks : [DisbursedStock];

  constructor(private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getDisbursedStock();
  }

  getDisbursedStock(){
    this.inventoryHttpService.getDisbursedStock().subscribe(stocks => {
      console.log(stocks);
      this.disbursedStocks = stocks;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

}
