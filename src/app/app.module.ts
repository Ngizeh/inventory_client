import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { CreateOrdersComponent } from './create-orders/create-orders.component';
import { TableOrdersComponent } from './table-orders/table-orders.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { StockComponent } from './stock/stock.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DisburseComponent } from './disburse/disburse.component';
import { DisbursedStockComponent } from './disbursed-stock/disbursed-stock.component';
import { ReturnsComponent } from './returns/returns.component';
import { ReturnedStockComponent } from './returned-stock/returned-stock.component';
import { AdminComponent } from './admin/admin.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { TransactionComponent } from './transaction/transaction.component';
import { ListTransactionsComponent } from './list-transactions/list-transactions.component';
import { EditOrdersComponent } from './edit-orders/edit-orders.component';
import { EditDisbursedComponent } from './edit-disbursed/edit-disbursed.component';
import { EditTransactionComponent } from './edit-transaction/edit-transaction.component';
import { EditReturnsComponent } from './edit-returns/edit-returns.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import {AuthenticationService} from "./services/authentication.service";
import {ToasterModule} from "angular2-toaster/angular2-toaster";
import {UserService} from "./services/user.service";
import { ProductComponent } from './product/product.component';
import {InventoryService} from "./services/inventory.service";
import { CreateProductComponent } from './create-product/create-product.component';
import { ExpiryComponent } from './expiry/expiry.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    CreateOrdersComponent,
    TableOrdersComponent,
    TopNavbarComponent,
    StockComponent,
    DisburseComponent,
    DisbursedStockComponent,
    ReturnsComponent,
    ReturnedStockComponent,
    AdminComponent,
    ViewUsersComponent,
    TransactionComponent,
    ListTransactionsComponent,
    EditOrdersComponent,
    EditDisbursedComponent,
    EditTransactionComponent,
    EditReturnsComponent,
    EditUsersComponent,
    ProductComponent,
    CreateProductComponent,
    ExpiryComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule,
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'create_orders',
        component: CreateOrdersComponent
      },
      {
        path: 'stock',
        component: StockComponent
      },
      {
        path: 'disburse',
        component: DisburseComponent
      },
      {
        path: 'disbursed',
        component: DisbursedStockComponent
      },
      {
        path: 'return',
        component: ReturnedStockComponent
      },
      {
        path: 'returns',
        component: ReturnsComponent
      },
      {
        path: 'admin',
        component: AdminComponent
      },
      {
      path: 'view',
    component: ViewUsersComponent
     },
      {
      path: 'transaction',
    component: TransactionComponent
      },
      {
        path: 'list',
        component: ListTransactionsComponent
      },
      {
        path: 'edit',
        component: EditOrdersComponent
      },
      {
      path: 'edit_disburse',
     component: EditDisbursedComponent
      },
      {
        path: 'edit_transaction',
        component: EditTransactionComponent
      },
      {
        path: 'edit_returns',
        component: EditReturnsComponent
      },
      {
        path: 'edit_users',
        component: EditUsersComponent
      },
      {
        path: 'products',
        component: ProductComponent
      },
      {
        path: 'create_products',
        component: CreateProductComponent
      },
      {
        path: 'update_expiry/:orderId',
        component: ExpiryComponent
      }
    ])
  ],
  providers: [InventoryService, UserService, AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
