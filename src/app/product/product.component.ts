import { Component, OnInit } from '@angular/core';
import {Product} from "../interfaces/product";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit{

  products : [Product];

  constructor(private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    this.inventoryHttpService.getProducts().subscribe(products => {
      this.products = products;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }
}
