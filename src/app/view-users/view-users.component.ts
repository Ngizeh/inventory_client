import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {User} from "../interfaces/user";
import {ToasterService} from "angular2-toaster/angular2-toaster";

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  users       : [User];

  constructor(private usersHttpService : UserService,
              private toasterService : ToasterService
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.usersHttpService.getUsers().subscribe(users => {
      this.users = users;
    }, error => {
      this.toasterService.pop('error', 'Error', error)
    });
  }



}
