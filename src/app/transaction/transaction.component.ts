import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";
import {Order} from "../interfaces/order";


@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  orders : [Order];

  transactionForm : FormGroup;

  orderId   : FormControl;

  cost : FormControl;

  invoiceNumber : FormControl;

  transactionDate : FormControl;


  constructor(private builder: FormBuilder,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getOrders();
    this.makeTransactionForm();
  }


  getOrders(){
    this.inventoryHttpService.getOrders().subscribe(orders => {
      this.orders = orders;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

  createTransaction(transactionData){
    this.inventoryHttpService.createTransaction(transactionData).subscribe(data => {
      this.toasterService.pop('success', 'Created',  data.message);
      this.transactionForm.reset();
    }, error => {
      error.forEach(err => {
        this.toasterService.pop('error', 'Error', err);
      });
    })
  }

  makeTransactionForm(){
    this.orderId = new FormControl('', [
      Validators.required
    ]);
    this.cost = new FormControl('', [
      Validators.required
    ]);

    this.invoiceNumber = new FormControl('', [
      Validators.required
    ]);

    this.transactionDate = new FormControl('', [
      Validators.required
    ]);

    this.transactionForm = this.builder.group({
      orderId   : this.orderId,
      cost   : this.cost,
      invoiceNumber  : this.invoiceNumber,
      transactionDate  : this.transactionDate
    });
  }

}
