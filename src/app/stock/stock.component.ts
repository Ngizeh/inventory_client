import { Component, OnInit } from '@angular/core';
import {Order} from "../interfaces/order";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  stocks : [Order];

  constructor(private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getStocks();
  }

  getStocks(){
    this.inventoryHttpService.getStocks().subscribe(stocks => {
      this.stocks = stocks;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

}
