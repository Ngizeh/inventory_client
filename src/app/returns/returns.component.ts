import { Component, OnInit } from '@angular/core';
import {Stock} from "../interfaces/stock";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";

@Component({
  selector: 'app-returns',
  templateUrl: './returns.component.html',
  styleUrls: ['./returns.component.css']
})
export class ReturnsComponent implements OnInit {

  stocks : [Stock];

  returnForm : FormGroup;

  stockId   : FormControl;

  quantity : FormControl;

  returnDate : FormControl;

  constructor(private builder: FormBuilder,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getStocks();
    this.makeReturnForm();
  }

  returnStock(returnData){
    this.inventoryHttpService.returnStock(returnData).subscribe(data => {
      this.toasterService.pop('success', 'Created',  data.message);
      this.returnForm.reset();
    }, error => {
      error.forEach(err => {
        this.toasterService.pop('error', 'Error', err);
      });
    })
  }

  getStocks(){
    this.inventoryHttpService.getStocks().subscribe(stocks => {
      this.stocks = stocks;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

  makeReturnForm(){
    this.stockId = new FormControl('', [
      Validators.required
    ]);
    this.quantity = new FormControl('', [
      Validators.required
    ]);

    this.returnDate = new FormControl('', [
      Validators.required
    ]);

    this.returnForm = this.builder.group({
      stockId : this.stockId,
      quantity        : this.quantity,
      returnDate       : this.returnDate
    });
  }
}
