import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppHttp} from "../app-http";
import {base_url} from "../app.constants";
import {Observable} from "rxjs/Rx";
import {User} from "../interfaces/user";

@Injectable()
export class UserService extends AppHttp{

  constructor(private http : Http) {
    super();
  }

  getUsers(): Observable<[User]> {
    return this.http.get(`${base_url}users`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }
}
