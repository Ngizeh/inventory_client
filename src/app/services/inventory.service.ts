import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppHttp} from "../app-http";
import {base_url} from "../app.constants";
import {Observable} from "rxjs/Rx";
import {Product} from "../interfaces/product";
import {User} from "../interfaces/user";
import {Order} from "../interfaces/order";
import {Stock} from "../interfaces/stock";
import {Transaction} from "../interfaces/transaction";
import {DisbursedStock} from "../interfaces/disbursed-stock";
import {Department} from "../interfaces/department";
import {ReturnedStock} from "../interfaces/return";

@Injectable()
export class InventoryService extends AppHttp{

  constructor(private http : Http) {
    super();
  }

  getProducts(): Observable<[Product]> {
    return this.http.get(`${base_url}inventories/products`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getOrders(): Observable<[Order]> {
    return this.http.get(`${base_url}inventories/orders`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getStocks(): Observable<[Stock]> {
    return this.http.get(`${base_url}inventories/orders/stock`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }


  getTransactions(): Observable<[Transaction]> {
    return this.http.get(`${base_url}inventories/transactions`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getDisbursedStock(): Observable<[DisbursedStock]> {
    return this.http.get(`${base_url}inventories/disburse`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getReturnedStock(): Observable<[ReturnedStock]> {
    return this.http.get(`${base_url}inventories/return`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  returnStock(returnData): Observable<any>{
    return this.http.post(`${base_url}inventories/return`, returnData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  createTransaction(transactionData): Observable<any>{
    return this.http.post(`${base_url}inventories/transactions`, transactionData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }


  getSuppliers(): Observable<[User]> {
    return this.http.get(`${base_url}roles/suppliers`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  createProduct(productData): Observable<any>{
    return this.http.post(`${base_url}inventories/products`, productData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  createOrder(orderData): Observable<any>{
    return this.http.post(`${base_url}inventories/orders`, orderData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  updateExpiry(expiryDate): Observable<any>{
    return this.http.put(`${base_url}inventories/expiry`, expiryDate)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  disburse(disburseData): Observable<any>{
    return this.http.post(`${base_url}inventories/disburse`, disburseData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }


  stock(stockData): Observable<Order>{
    return this.http.put(`${base_url}inventories/orders/stock`, stockData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getDepartments(): Observable<[Department]> {
    return this.http.get(`${base_url}inventories/departments`, this.getOptions())
        .map(this.extractData)
        .catch(this.handleErrors);
  }
}
