import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppHttp} from "../app-http";
import {base_url} from "../app.constants";
import {Observable} from "rxjs/Rx";
import {Login} from "../interfaces/login";
import {Role} from "../interfaces/role";


@Injectable()
export class AuthenticationService extends AppHttp{

  constructor(private http : Http) {
    super();
  }

  registerUser(userData): Observable<any>{
    return this.http.post(`${base_url}auth/register-user`, userData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  login(loginData): Observable<Login>{
    return this.http.post(`${base_url}auth/login`, loginData)
        .map(this.extractData)
        .catch(this.handleErrors);
  }

  getRoles(): Observable<[Role]>{
    return this.http.get(`${base_url}roles`)
        .map(this.extractData)
        .catch(this.handleErrors);
  }
}
