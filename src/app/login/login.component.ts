import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {AuthenticationService} from "../services/authentication.service";
import {ToasterService} from "angular2-toaster";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;

  email: FormControl;

  password: FormControl;

  role : string;

  constructor(private builder: FormBuilder,
              private authenticationService: AuthenticationService,
              private toaster : ToasterService,
              public router : Router) { }

  ngOnInit() {
    this.makeLoginForm();
  }

  login(loginData) {
    console.log(loginData);
    this.authenticationService.login(loginData).subscribe(data => {
      this.role = data.role
      this.setAuthInfo(data);
    }, error => {
      error.forEach(err => {
        this.toaster.pop('error', 'Error', err);
      });
    }, () => {
      this.router.navigate(['/dashboard']);
    })
  }

  makeLoginForm(){
    this.email = new FormControl('', [
      Validators.required
    ]);
    this.password = new FormControl('', [
      Validators.required
    ]);
    this.loginForm = this.builder.group({
      email: this.email,
      password: this.password
    });
  }

  setAuthInfo(data){
    localStorage.setItem('name', data.name);
    localStorage.setItem('token', data.token);
    localStorage.setItem('roleDescription', data.roleDescription);
    localStorage.setItem('role', data.role);
  }
}
