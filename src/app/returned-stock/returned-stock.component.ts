import { Component, OnInit } from '@angular/core';
import {ReturnedStock} from "../interfaces/return";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";

@Component({
  selector: 'app-returned-stock',
  templateUrl: './returned-stock.component.html',
  styleUrls: ['./returned-stock.component.css']
})
export class ReturnedStockComponent implements OnInit {

  returnedStocks : [ReturnedStock];


  constructor(private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getReturnedStock();
  }

  getReturnedStock(){
    this.inventoryHttpService.getReturnedStock().subscribe(returns => {
      console.log(returns);
      this.returnedStocks = returns;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

}
