import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnedStockComponent } from './returned-stock.component';

describe('ReturnedStockComponent', () => {
  let component: ReturnedStockComponent;
  let fixture: ComponentFixture<ReturnedStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnedStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnedStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
