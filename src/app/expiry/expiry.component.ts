import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-expiry',
  templateUrl: './expiry.component.html',
  styleUrls: ['./expiry.component.css']
})
export class ExpiryComponent implements OnInit {

  expiryForm : FormGroup;

  expiryDate : FormControl;

  orderId : number;

  constructor(public route: ActivatedRoute,
              private builder: FormBuilder,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) {
    this.route.params.subscribe(params => {
      this.orderId = params['orderId']
    })
  }

  ngOnInit() {
    this.makeExpiryForm();
  }

  makeExpiryForm(){

    this.expiryDate = new FormControl('', [
      Validators.required
    ]);


    this.expiryForm = this.builder.group({
      expiryDate : this.expiryDate
    });
  }

  updateExpiryDate(expiryDate){
    expiryDate.orderId = this.orderId;
    this.inventoryHttpService.updateExpiry(expiryDate).subscribe(data => {
      this.toasterService.pop('success', 'Created',  data.message);
      this.expiryForm.reset();
    }, error => {
      error.forEach(err => {
        this.toasterService.pop('error', 'Error', err);
      });
    })

  }

}
