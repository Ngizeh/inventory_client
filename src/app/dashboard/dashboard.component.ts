import { Component, OnInit } from '@angular/core';
import {Order} from "../interfaces/order";
import {ActivatedRoute, Router} from "@angular/router";

import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  orders : [Order];

  constructor(public route: ActivatedRoute,
              public router : Router,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getOrders();
  }

  stock(stockData){
    this.inventoryHttpService.stock(stockData).subscribe(data => {
      this.orders.forEach((order, index) => {
        if (order.orderId == data.orderId) {
          this.orders[index] = data;
        }
      });
      this.toasterService.pop('success', 'Created',  'Order Stocked');
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

  getOrders(){
    this.inventoryHttpService.getOrders().subscribe(orders => {
      this.orders = orders;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

}
