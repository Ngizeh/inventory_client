import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../services/authentication.service";
import {ToasterService} from "angular2-toaster";
import {Role} from "../interfaces/role";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  roles : [Role];

  userForm : FormGroup;

  name : FormControl;

  email : FormControl;

  role  : FormControl;

  password : FormControl;

  constructor(private builder: FormBuilder,
              private authenticationService: AuthenticationService,
              private toaster : ToasterService) { }

  ngOnInit() {
    this.getRoles();
    this.makeUserForm();
  }

  getRoles(){
    this.authenticationService.getRoles().subscribe(roles => {
        this.roles = roles;
    }, error => {
      this.toaster.pop('error', 'Error', error.message);
    })
  }

  createUser(userData){
    this.authenticationService.registerUser(userData).subscribe(data => {
      this.toaster.pop('success', 'Created',  data.message);
    }, error => {
      error.forEach(err => {
        this.toaster.pop('error', 'Error', err);
      });
    })
  }

  makeUserForm(){
    this.name = new FormControl('', [
      Validators.required
    ]);
    this.email = new FormControl('', [
      Validators.required
    ]);

    this.role = new FormControl('', [
      Validators.required
    ]);

    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(8)

    ]);

    this.userForm = this.builder.group({
      name : this.name,
      email: this.email,
      role : this.role,
      password: this.password
    });
  }

}
