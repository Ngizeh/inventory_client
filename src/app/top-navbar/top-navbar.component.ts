import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {

  role = localStorage.getItem('role');
  role_description : string = localStorage.getItem('roleDescription');
  name : string = localStorage.getItem('name');

  constructor( public router : Router) { }

  ngOnInit() {
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/'])
  }

}
