import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ToasterService} from "angular2-toaster";
import {InventoryService} from "../services/inventory.service";
import {User} from "../interfaces/user";



@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  productForm : FormGroup;

  name : FormControl;

  description : FormControl;

  price  : FormControl;

  supplier : FormControl;

  suppliers : [User];

  constructor(private builder: FormBuilder,
              private inventoryHttpService : InventoryService,
              private toaster : ToasterService) { }

  ngOnInit() {
    this.getSuppliers();
    this.makeProductForm();
  }

  getSuppliers(){
    this.inventoryHttpService.getSuppliers().subscribe(suppliers => {
      this.suppliers = suppliers;
    }, error => {
      this.toaster.pop('error', 'Error', error.message);
    })
  }

  createProduct(productData){
    this.inventoryHttpService.createProduct(productData).subscribe(data => {
      this.toaster.pop('success', 'Created',  data.message);
      this.productForm.reset();
    }, error => {
      error.forEach(err => {
        this.toaster.pop('error', 'Error', err);
      });
    })
  }

  makeProductForm(){
    this.name = new FormControl('', [
      Validators.required
    ]);
    this.description = new FormControl('', [
      Validators.required
    ]);

    this.price = new FormControl('', [
      Validators.required
    ]);

    this.supplier = new FormControl('', [
      Validators.required,
    ]);

    this.productForm = this.builder.group({
      name        : this.name,
      description : this.description,
      price : this.price,
      supplier: this.supplier
    });
  }
}
