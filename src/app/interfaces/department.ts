export interface Department {
    departmentId? : number,
    departmentName?: string,
    departmentDescription? : string,
    createdAt?   : string,
    message?     : string
}
