export interface DisbursedStock {
    disbursedStockId ? : number,
    stockId ?          : number,
    departmentName ?   : string,
    productName ?      : string,
    quantity    ?      : number,
    recipient  ?       : string,
    createdAt  ?       : string
}
