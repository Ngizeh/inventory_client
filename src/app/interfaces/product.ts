export interface Product {
    productId ?   : number,
    name ?        : string,
    description ? : string,
    price ?       : number,
    supplier ?    : string,
    createdAt ?   : string
}
