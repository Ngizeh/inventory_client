export interface Login {
    name? : string,
    token?: string,
    role?:  string,
    roleDescription?: string
}
