export interface UserRegistration {
    token?: string,
    role?:  string,
    roleDescription?: string
}
