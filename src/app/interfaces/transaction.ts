export interface Transaction {
    transactionId ? : number,
    orderId ?       : number,
    cost ?          : number,
    invoiceNumber ? : string,
    transactionDate ? : string
}
