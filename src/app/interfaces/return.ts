export interface ReturnedStock {
    returnedStockId ? : number,
    stockId ?          : number,
    productName ?      : string,
    quantity    ?      : number,
    returnDate  ?       : string,
    createdAt  ?       : string
}
