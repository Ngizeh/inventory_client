export interface Order {
    orderId ? : number,
    productName ? : string,
    productSupplier ? : string,
    quantity        ? : number,
    status   ?        : string,
    expiryDate ?      : string,
    createdAt       ? : string,
    deliveryDate   ?  : string
}
