export interface Stock {
    stockId ? : number,
    productName ? : string,
    quantity  ?   : number,
    alertQuantity ? : number
}
