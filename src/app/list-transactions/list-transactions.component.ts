import { Component, OnInit } from '@angular/core';
import {Transaction} from "../interfaces/transaction";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";

@Component({
  selector: 'app-list-transactions',
  templateUrl: './list-transactions.component.html',
  styleUrls: ['./list-transactions.component.css']
})
export class ListTransactionsComponent implements OnInit {

  transactions : [Transaction];

  constructor(private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getTransactions();
  }

  getTransactions(){
    this.inventoryHttpService.getTransactions().subscribe(transactions => {
      this.transactions = transactions;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

}
