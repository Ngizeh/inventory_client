import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {Product} from "../interfaces/product";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {InventoryService} from "../services/inventory.service";




@Component({
  selector: 'app-create-orders',
  templateUrl: './create-orders.component.html',
  styleUrls: ['./create-orders.component.css']
})
export class CreateOrdersComponent implements OnInit {

  products : [Product];

  orderForm : FormGroup;

  product   : FormControl;

  quantity : FormControl;

  deliveryDate : FormControl;


  constructor(private builder: FormBuilder,
              private toasterService : ToasterService,
              private inventoryHttpService : InventoryService) { }

  ngOnInit() {
    this.getProducts();
    this.makeOrderForm();
  }

  getProducts(){
    this.inventoryHttpService.getProducts().subscribe(products => {
      this.products = products;
    }, error => {
      this.toasterService.pop('error', 'Error', error.message)
    });
  }

  createOrder(orderData){
    this.inventoryHttpService.createOrder(orderData).subscribe(data => {
      this.toasterService.pop('success', 'Created',  data.message);
      this.orderForm.reset();
    }, error => {
      error.forEach(err => {
        this.toasterService.pop('error', 'Error', err);
      });
    })
  }

  makeOrderForm(){
    this.quantity = new FormControl('', [
      Validators.required
    ]);
    this.deliveryDate = new FormControl('', [
      Validators.required
    ]);

    this.product = new FormControl('', [
      Validators.required
    ]);

    this.orderForm = this.builder.group({
      deliveryDate : this.deliveryDate,
      product      : this.product,
      quantity        : this.quantity
    });
  }

}
