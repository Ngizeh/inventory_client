import { InventoryClientPage } from './app.po';

describe('inventory-client App', () => {
  let page: InventoryClientPage;

  beforeEach(() => {
    page = new InventoryClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
